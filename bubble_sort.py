a = [int(x) for x in input("Enter list of numbers: ").split()]
l = len(a)
change = 1
while (change != 0):
    change = 0
    for i in range(0,l-1):
        if (a[i] > a[i+1]):
            b = a[i]
            c = a[i+1]
            a[i] = c
            a[i+1] = b
            change += 1
print(a)
