from Crypto.Util import number
from Crypto.Random import random

n = int(input("Input key length: "))
p = number.getPrime(n, randfunc=None)
q = number.getPrime(n, randfunc=None)

N = p*q
phi = (p-1)*(q-1)
e = 65537

def gcd(x,y):
    s = 0
    t = 1
    r = y
    s0 = 1
    t0 = 0
    r0 = x
    while (r != 0):
        z = divmod(r0,r)
        q = z[0]
        (r0, r) = (r, r0 - q*r)
        (s0, s) = (s, s0 - q*s)
        (t0, t) = (t, t0 - q*t)
    return s0

g = gcd(e, phi)
if (g < 0):
    d = g + phi
else:
    d = g

print("Public key modulus is: ", N)
print("Public Key exponent is: ", e)
print("Private key exponent is: ", d)
