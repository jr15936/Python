import string

#set up dictionaries where letters correspond to numbers and vice-versa
v = dict()
u = dict()
for index, letter in enumerate(string.ascii_uppercase):
    v[letter] = index
    u[index] = letter
u[26] = " "
v[" "] = 26

#Encrypt function
def enc(m,k):
    kl = len(k)
    ml = len(m)
    c = []
    x = divmod(ml,kl)
    if (x[0]>0):
        k = k*x[0]
    for i in range(0,x[1]):
        k = k + k[i]
    for i in range(0,ml):
        y = (v[m[i]] + v[k[i]])%27
        c.append(u[y])
    return ''.join(c)

#Decrypt function
def dec(m,k):
    kl = len(k)
    ml = len(m)
    c = []
    x = divmod(ml,kl)
    if (x[0]>0):
        k = k*x[0]
    for i in range(0,x[1]):
        k = k + k[i]
    for i in range(0,ml):
        y = (v[m[i]] - v[k[i]])%27
        c.append(u[y])
    return ''.join(c)

k = input("Enter key word in all caps: ")
t = input("Decrypt or Encrypt message? [enc/dec]: ")
m = input("Input message in all caps: ")
if (t == "dec"):
    print(dec(m,k))
elif (t == "enc"):
    print(enc(m,k))
else:
    print("error")
