#counts number of vowels in a string
x = input("Input string: ")
l = len(x)
a = 0
e = 0
i = 0
o = 0
u = 0
for j in range(0,l):
    if (x[j] == "a"):
        a = a + 1
    elif (x[j] == "e"):
        e = e + 1
    elif (x[j] == "i"):
        i = i + 1
    elif (x[j] == "o"):
        o = o + 1
    elif (x[j] == "u"):
        u = u + 1
print("Number of \"a\"s is ",a)
print("Number of \"e\"s is ",e)
print("Number of \"i\"s is ",i)
print("Number of \"o\"s is ",o)
print("Number of \"u\"s is ",u)
print("Total number of vowels is ",a+e+i+o+u)                          
