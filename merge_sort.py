def merge(l,r):
    s = []
    i = 0
    j = 0
    len_l = len(l)
    len_r = len(r)
    while ((i < len_l) and (j < len_r)):
        if (l[i] <= r[j]):
            s.append(l[i])
            i += 1
        else:
            s.append(r[j])
            j += 1
    if (i < j):
        for k in range(i,len_l):
            s.append(l[k])
    elif (j < i):
        for k in range(j,len_r):
            s.append(r[k])
    return s

def sort(x):
    if (len(x) < 2):
        return x
    l = []
    r = []
    for i in range(0,int(len(x)/2)):
        l.append(x[i])
    for i in range(int(len(x)/2),len(x)):
        r.append(x[i])
    l = sort(l)
    r = sort(r)
    return merge(l,r)

a = [int(x) for x in input("Enter list of numbers: ").split()]
print(sort(a))
