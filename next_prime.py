def is_prime(x):
    y = int(x**0.5) + 1
    p = 0
    for i in range(2,y):
        if (x%i == 0):
            p = 1
    if (p == 0):
        return True
    else:
        return False

def next(x):
    x = x + 1
    if (is_prime(x)):
        return x
    else:
        x = next(x)
        return x    

t = 1
i = "y"
while(i == "y"):
    i = input("Get next prime number? [y/n]: ")
    w = next(t)
    print(w)
    t = w
