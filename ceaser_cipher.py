import string

#set up dictionaries where letters correspond to numbers and vice-versa
v = dict()
u = dict()
for index, letter in enumerate(string.ascii_uppercase):
    v[letter] = index
    u[index] = letter
u[26] = " "
v[" "] = 26

#Encrypt function
def enc(m,k):
    n = []
    l = len(m)
    for i in range(0,l):
            x = v[m[i]]
            y = (x+k)%27
            n.append(u[y])
    return ''.join(n)

#Decrypt function
def dec(m,k):
    n = []
    l = len(m)
    for i in range(0,l):
            x = v[m[i]]
            y = (x-k)%27
            n.append(u[y])
    return ''.join(n)

k = int(input("Enter Key: "))
t = input("Decrypt or Encrypt message? [enc/dec]: ")
m = input("Input message in all caps: ")
if (t == "dec"):
    print(dec(m,k))
elif (t == "enc"):
    print(enc(m,k))
else:
    print("error")
