a = [int(x) for x in input("Enter list of numbers: ").split()]
def partition(x):
    a = []
    b = []
    l = len(x)
    p = x[int(l/2)]
    for i in range(0,l):
        if (x[i] < p):
            a.append(x[i])
        elif (x[i] > p):
            b.append(x[i])
        elif ((i != int(l/2)) & (p == x[i])):
            a.append(x[i])
    return [a,b,p]

def quick(x,c):
    if (x == []):
        return c
    y = partition(x)
    quick(y[0],c)
    c.append(y[2])
    quick(y[1],c)
    return c
y = quick(a,[])
print(y)
